# Jon's Personal Theme

#use extended color pallete if available
if [[ $terminfo[colors] -ge 256 ]]; then
    turquoise="%F{81}"
    orange="%F{166}"
    purple="%F{135}"
    hotpink="%F{161}"
    limegreen="%F{118}"
else
    turquoise="%F{cyan}"
    orange="%F{yellow}"
    purple="%F{magenta}"
    hotpink="%F{red}"
    limegreen="%F{green}"
fi

ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_PREFIX=" %{$reset_color%}(%{$turquoise%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%})"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$purple%}●"

STAFF_CURRENT_MACHINE="%{$orange%}%m%{$reset_color%}"

STAFF_CURRENT_USER="%{$purple%}%n%{$reset_color%}"

STAFF_CURRENT_DIR="%{$limegreen%}%~\$(git_prompt_info)%{$reset_color%}"

STAFF_CURRENT_TIME="[%{$fg[yellow]%}%D% , %{$fg[blue]%}%T% %{$reset_color%}]"

RPROMPT="$STAFF_CURRENT_TIME"
#PROMPT=$'
#$STAFF_CURRENT_USER at $STAFF_CURRENT_MACHINE in $STAFF_CURRENT_DIR
#$ '
PROMPT=$'\n'"$STAFF_CURRENT_USER at $STAFF_CURRENT_MACHINE in $STAFF_CURRENT_DIR "$'\n'"$ "
