#!/usr/bin/env python

import os

print "Configuring server with a respectable .vimrc..."

with open('.zshrc', 'r') as f:
    data = f.readlines()

for line in data:
    if 'alias' in line and 'ssh' in line and '@' in line:
        user = line[line.index('ssh ') + 4 : line.index('@')]
        #print user
        hostname = line[line.index('@') + 1 : -2]
        print hostname
        os.system('scp server.vimrc ' + user + '@' + hostname + ':/' + user + '/.vimrc')
