" .vimrc used for server configurations; no plugins
" -------------------------------------------------

set nocompatible              " be iMproved, required
filetype off                  " required

filetype plugin indent on    " required

syntax enable
set number

" Automatically indent (smartly if possible)
set autoindent
set smartindent

" Use spaces instead of tabs (smartly if possible)
set expandtab
set smarttab

" 1 tab == 4 spaces
set shiftwidth=2
set tabstop=2

" Turn off folding by default
set nofoldenable

" Better path completion
set wildmode=longest,list,full
set wildmenu

set showmatch

" Force backspace to function properly
set backspace=indent,eol,start

" Highlight in search
set hlsearch

" No sounds on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Turn backup off
set nobackup
set nowb
set noswapfile

" Map <leader> to ',' for easier typing
let mapleader = ','

" Disable the arrow keys, because who uses these anyway?
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
