set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter' " Comment with <leader> cc and uncomment with <leader> uc
Plugin 'marijnh/tern_for_vim' " Additional autocompletion for YouCompleteMe
Plugin 'kien/ctrlp.vim'
Plugin 'bling/vim-airline'
Plugin 'jiangmiao/auto-pairs' " Automatically pairs characters like parentheses, quotes, etc
Plugin 'digitaltoad/vim-jade'
Plugin 'bling/vim-bufferline'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'mhinz/vim-startify'
Plugin 'tpope/vim-unimpaired' " yo before pasting from clipboard
Plugin 'vim-scripts/bufkill.vim' " Delete buffer without closing the window
Plugin 'terryma/vim-multiple-cursors'
Plugin 'pangloss/vim-javascript'
Plugin 'christoomey/vim-tmux-navigator' " Navigate between vim and tmux panes
Plugin 'mustache/vim-mustache-handlebars'
Plugin 'scrooloose/syntastic'
Plugin 'Yggdroot/indentLine'
Plugin 'fatih/vim-go'
Plugin 'keith/swift.vim'

Bundle 'flazz/vim-colorschemes'
Bundle 'jistr/vim-nerdtree-tabs'
Bundle 'Valloric/YouCompleteMe'
Bundle 'plasticboy/vim-markdown'
Bundle 'matze/vim-move'

call vundle#end()            " required
filetype plugin indent on    " required
" *------------- End Vundle ---------------------------------*

" *----------- Jon's Customization --------------------------*
syntax enable
set number
set relativenumber

" Automatically indent (smartly if possible)
set autoindent
set smartindent

" Use spaces instead of tabs (smartly if possible)
set expandtab
set smarttab

" 1 tab == 4 spaces
set shiftwidth=2
set tabstop=2

" Turn off folding by default
set nofoldenable

" Better path completion
set wildmode=longest,list,full
set wildmenu

set showmatch

" Force backspace to function properly
set backspace=indent,eol,start

" Highlight in search
set hlsearch

" No sounds on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Turn backup off
set nobackup
set nowb
set noswapfile

" Open new splits to the right and bottom
set splitbelow
set splitright

" Editor colors
set background=dark
colorscheme molokai

highlight clear SignColumn
highlight VertSplit    ctermbg=236
highlight ColorColumn  ctermbg=237
highlight LineNr       ctermbg=236 ctermfg=240
highlight CursorLineNr ctermbg=236 ctermfg=240
highlight CursorLine   ctermbg=236
highlight StatusLineNC ctermbg=238 ctermfg=0
highlight StatusLine   ctermbg=240 ctermfg=12
highlight IncSearch    ctermbg=3   ctermfg=1
highlight Search       ctermbg=1   ctermfg=3
highlight Visual       ctermbg=3   ctermfg=0
highlight Pmenu        ctermbg=240 ctermfg=12
highlight PmenuSel     ctermbg=3   ctermfg=1
highlight SpellBad     ctermbg=0   ctermfg=1

" Map <leader> to ',' for easier typing
let mapleader = ','

" CtrlP Mapping
let g:ctrlp_map = '<leader>t'

" Swap colon and semicolon because we all hate that shift key
nnoremap ; :
vnoremap ; :

" Show hidden files in NERDTree
let NERDTreeShowHidden = 1

" Show hidden files with ctrlp
let g:ctrlp_show_hidden = 1

" Disable the arrow keys, because who uses these anyway?
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" Move to next and previous buffers
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprev<CR>

" Better pane navigation
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Reselect text that was just pasted
nnoremap <leader>v V`]

" Remap the move modifier key to ctrl
let g:move_key_modifier = 'C'

" Disable markdown folding
let g:vim_markdown_folding_disabled=1

" Automatically close the scratch preview
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

" YouCompleteMe fallback config
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'

" *----------- Startify Options ----------------------------*

let g:startify_custom_footer = [
      \ '',
      \ '',
      \ '       _/      _/  _/                        _/_/          _/    ',
      \ '      _/      _/      _/_/_/  _/_/        _/    _/      _/  _/   ',
      \ '     _/      _/  _/  _/    _/    _/        _/_/        _/  _/    ',
      \ '      _/  _/    _/  _/    _/    _/      _/    _/      _/  _/     ',
      \ '       _/      _/  _/    _/    _/        _/_/    _/    _/        ',
      \ '',
      \ '',
      \ ]



" Allows Startify to work with NERDTree
autocmd VimEnter *
      \ if !argc() |
      \   Startify |
      \   NERDTree |
      \   execute "normal \<c-w>w" |
      \ endif

" coloring
hi StartifyBracket ctermfg=240
hi StartifyNumber  ctermfg=215
hi StartifyPath    ctermfg=245
hi StartifySlash   ctermfg=240
hi StartifySpecial ctermfg=240
hi StartifyHeader  ctermfg=114
hi StartifyFooter  ctermfg=240

let g:startify_session_persistence = 1

" *----------- vim-airline  Options ----------------------------*

" Always show the statusline
set laststatus=2
