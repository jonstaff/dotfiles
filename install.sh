#!/bin/sh

# Installs .dotfiles where needed.
#
# Written by:  Jonathon Staff
# Date:        February 4, 2015

echo "Installing .dotfiles..."

[ -f "~/.vimrc" ] && rm ~/.vimrc
cp .vimrc ~/.vimrc

[ -f "~/.ideavimrc" ] && rm ~/.ideavimrc
cp .ideavimrc ~/.ideavimrc

[ -f "~/.gitconfig" ] && rm ~/.gitconfig
cp .gitconfig ~/.gitconfig

[ -f "~/.zshrc" ] && rm ~/.zshrc
cp .zshrc ~/.zshrc

[ -f "~/.tmux.conf" ] && rm ~/.tmux.conf
cp .tmux.conf ~/.tmux.conf

echo ".dotfiles installed."
