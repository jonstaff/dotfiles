" Welcome to my frankenstein vim config. Updated when I started at Envoy and
" in serious need of a cleaning... (much of this was just copied from Viddi's)

call plug#begin('~/.vim/plugged')

" LSP
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
Plug 'sheerun/vim-polyglot' " Support for most languages
Plug 'tomtom/tcomment_vim' " Multi-language commenting
Plug 'jiangmiao/auto-pairs' " Automatically open/close pairs

" File manager
Plug 'francoiscabrol/ranger.vim'
Plug 'rbgrouleff/bclose.vim'

" Fuzzy search files
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Status bar
Plug 'itchyny/lightline.vim'
Plug 'itchyny/vim-gitbranch' " Display git branch on status bar
Plug 'ap/vim-buftabline' " Display buffers on tab bar

" Colorschemes
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'kien/rainbow_parentheses.vim'

Plug 'tpope/vim-obsession'

call plug#end()

" Plug 'scrooloose/nerdtree'
" Plug 'scrooloose/nerdcommenter' " Comment with <leader> cc and uncomment with <leader> uc
" Plugin 'kien/ctrlp.vim'
" Plugin 'bling/vim-airline'
" Plugin 'jiangmiao/auto-pairs' " Automatically pairs characters like parentheses, quotes, etc
" Plugin 'digitaltoad/vim-jade'
" Plugin 'bling/vim-bufferline'
" Plugin 'tpope/vim-fugitive'
" Plugin 'tpope/vim-surround'
" Plugin 'mhinz/vim-startify'
" Plugin 'tpope/vim-unimpaired' " yo before pasting from clipboard
" Plugin 'vim-scripts/bufkill.vim' " Delete buffer without closing the window
" Plugin 'terryma/vim-multiple-cursors'
" Plugin 'pangloss/vim-javascript'
" Plugin 'christoomey/vim-tmux-navigator' " Navigate between vim and tmux panes
" Plugin 'mustache/vim-mustache-handlebars'
" Plugin 'scrooloose/syntastic'
" Plugin 'Yggdroot/indentLine'
" Plugin 'fatih/vim-go'
" Plugin 'keith/swift.vim'

" Bundle 'flazz/vim-colorschemes'
" Bundle 'jistr/vim-nerdtree-tabs'
" Bundle 'Valloric/YouCompleteMe'
" Bundle 'plasticboy/vim-markdown'
" Bundle 'matze/vim-move'


" *----------- Jon's Customization --------------------------*
set number
set relativenumber

" Automatically indent (smartly if possible)
set autoindent
set smartindent

" Use spaces instead of tabs (smartly if possible)
set expandtab
set smarttab

" Default tabs to 2 spaces
set shiftwidth=2
set tabstop=2

" Turn off folding by default
set nofoldenable

" Better path completion
set wildmode=longest,list,full
set wildmenu

set showmatch

" Force backspace to function properly
set backspace=indent,eol,start

" --------- Search ---------
set hlsearch " Highlight in search
set ignorecase " Case insensitive search
set smartcase " If pattern has uppercase then case sensitive

" No sounds on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Turn backup off
set nobackup
set nowb
set noswapfile

" Open new splits to the right and bottom
set splitbelow
set splitright

" " Editor colors
" set background=dark
" colorscheme molokai
" 
" highlight clear SignColumn
" highlight VertSplit    ctermbg=236
" highlight ColorColumn  ctermbg=237
" highlight LineNr       ctermbg=236 ctermfg=240
" highlight CursorLineNr ctermbg=236 ctermfg=240
" highlight CursorLine   ctermbg=236
" highlight StatusLineNC ctermbg=238 ctermfg=0
" highlight StatusLine   ctermbg=240 ctermfg=12
" highlight IncSearch    ctermbg=3   ctermfg=1
" highlight Search       ctermbg=1   ctermfg=3
" highlight Visual       ctermbg=3   ctermfg=0
" highlight Pmenu        ctermbg=240 ctermfg=12
" highlight PmenuSel     ctermbg=3   ctermfg=1
" highlight SpellBad     ctermbg=0   ctermfg=1

" Map <leader> to ',' for easier typing
let mapleader = ','

" CtrlP Mapping
let g:ctrlp_map = '<leader>t'

" Swap colon and semicolon because we all hate that shift key
nnoremap ; :
vnoremap ; :

" Move to next and previous buffers
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprev<CR>

" Better pane navigation
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Reselect text that was just pasted
nnoremap <leader>v V`]

" Remap the move modifier key to ctrl
let g:move_key_modifier = 'C'

" Disable markdown folding
let g:vim_markdown_folding_disabled=1

" " *----------- Startify Options ----------------------------*
" 
" let g:startify_custom_footer = [
"       \ '',
"       \ '',
"       \ '       _/      _/  _/                        _/_/          _/    ',
"       \ '      _/      _/      _/_/_/  _/_/        _/    _/      _/  _/   ',
"       \ '     _/      _/  _/  _/    _/    _/        _/_/        _/  _/    ',
"       \ '      _/  _/    _/  _/    _/    _/      _/    _/      _/  _/     ',
"       \ '       _/      _/  _/    _/    _/        _/_/    _/    _/        ',
"       \ '',
"       \ '',
"       \ ]
" 
" 
" 
" " Allows Startify to work with NERDTree
" autocmd VimEnter *
"       \ if !argc() |
"       \   Startify |
"       \   NERDTree |
"       \   execute "normal \<c-w>w" |
"       \ endif
" 
" " coloring
" hi StartifyBracket ctermfg=240
" hi StartifyNumber  ctermfg=215
" hi StartifyPath    ctermfg=245
" hi StartifySlash   ctermfg=240
" hi StartifySpecial ctermfg=240
" hi StartifyHeader  ctermfg=114
" hi StartifyFooter  ctermfg=240
" 
" let g:startify_session_persistence = 1

" *----------- vim-airline  Options ----------------------------*

" Always show the statusline
set laststatus=2

" ========================================================================
"                                 LSP
" ========================================================================
" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
\  pumvisible() ? "\<C-n>" :
\  <SID>check_back_space() ? "\<TAB>" :
\  coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Search open buffers only
nmap <leader>s :CocList --interactive grep<CR>

" Use <c-space> for trigger completion.
inoremap <silent><expr> <C-SPACE> coc#refresh()

" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` for navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
vmap <leader>f <Plug>(coc-format-selected)
nmap <leader>f :Format<CR>

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
vmap <leader>a <Plug>(coc-codeaction-selected)
nmap <leader>a <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac <Plug>(coc-codeaction)
" Fix autofix problem of current line
" nmap <leader>qf <Plug>(coc-fix-current)

" Use `:Format` for format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` for fold current buffer
command! -nargs=? Fold :call CocAction('fold', <f-args>)

" ========================================================================
"                                TComment
" ========================================================================
nmap <leader>c :TComment<CR>
vmap <leader>c :TCommentBlock<CR>

" ========================================================================
"                                 Ranger
" ========================================================================
let g:ranger_command_override = 'ranger --cmd "set show_hidden=true"'
let g:ranger_map_keys = 0
map <C-t> :Ranger<CR>

" ========================================================================
"                                 FzF
" ========================================================================
" let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'
let $FZF_DEFAULT_COMMAND = 'rg --files --follow --hidden --glob "!{.git/*,*.pyc,*/Pods/*,*/node_modules/*}"'
" let g:fzf_layout = { 'down': '~20%' }
let g:fzf_layout = {
      \ 'window': {
        \ 'width': 0.8,
        \ 'height': 0.5,
        \ 'highlight': 'Comment',
        \ 'rounded': v:false
      \ }
\ }
let g:fzf_nvim_statusline = 0 " Disable statusline overwriting

nmap <leader>t :Files<CR>
nmap <leader>T :Buffers<CR>

" ========================================================================
"                            Window Management
" ========================================================================
nmap <leader>w :InteractiveWindow<CR>

" ========================================================================
"                             Themes & colors
" ========================================================================
if $TERM == "rxvt-unicode-256color" || $TERM == "screen-256color" || $COLORTERM == "gnome-terminal"
  set t_Co=256
endif

syntax enable
set background=dark
colorscheme dracula

" Remove background to keep consistent with xresources
hi Normal guibg=NONE ctermbg=NONE

" Theming for vim-buftabline plugin
" Gray 239 - Cyan 117 - Green 84 - Purple 141
hi BufTabLineFill ctermbg=239
hi BufTabLineCurrent ctermfg=84
hi BufTabLineHidden ctermfg=117 ctermbg=239

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" ========================================================================
"                                Lightline
" ========================================================================
let g:lightline = {
\  'colorscheme': 'dracula',
\  'active': {
\    'left': [
\      ['mode', 'paste'],
\      ['gitbranch', 'readonly', 'filename']
\    ]
\  },
\  'component_function': {
\    'gitbranch': 'gitbranch#name',
\    'readonly': 'LightlineReadonly',
\    'filename': 'LightlineFilename',
\    'cocstatus': 'coc#status'
\  }
\}

" Hide readonly label in help buffers
function! LightlineReadonly()
  return &readonly && &filetype !=# 'help' ? 'RO' : ''
endfunction

" Remove the bar between filename and modified sign
function! LightlineFilename()
  let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
  let modified = &modified ? ' +' : ''
  return filename . modified
endfunction
