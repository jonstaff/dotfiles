# see: https://github.com/ohmyzsh/ohmyzsh/issues/6835#issuecomment-390216875
ZSH_DISABLE_COMPFIX=true

# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

ZSH_THEME="staff"

HIST_STAMPS="yyyy-mm-dd"

plugins=(git)

source $ZSH/oh-my-zsh.sh
source /opt/homebrew/Cellar/z/1.9/etc/profile.d/z.sh

# User configuration

export GOPATH='/Users/jonathonstaff/go'
export PATH="/usr/local/bin:/opt/homebrew/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Users/jonathonstaff/bin:/usr/local/bin/virtualenvwrapper.sh:$GOPATH/bin"

# Preferred editor
export EDITOR='nvim'
export VISUAL='nvim'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ------------------------------------- *
# Generate .gitignore files
# ------------------------------------- *
function gi() { curl -s https://www.gitignore.io/api/$@ ; }

# ------------------------------------- *
# Generate Android .gitignore
# ------------------------------------- *
alias gi-android="gi osx,windows,android,intellij,eclipse"

# ------------------------------------- *
# Generate iOS .gitignore
# ------------------------------------- *
alias gi-ios="gi osx,objective-c,swift,c,appcode,xcode"

# ------------------------------------- *
# Make directory and cd into it
# ------------------------------------- *
function mkcd() { mkdir -p "$*"; cd "$*" }

# ------------------------------------- *
# iTerm2 settings
# ------------------------------------- *
export LC_ALL=en_US.UTF-8  
export LANG=en_US.UTF-8

# ------------------------------------- *
# Git Aliases
# ------------------------------------- *
alias gs="git status"
alias gd="git diff"
alias gcam="git commit -am"
alias gaa="git add -A :/"
alias gcm="git commit -m"
alias gf="git fetch"
alias gfo="git fetch origin"
alias gp="git push"
alias gpo="git push origin"
alias grh="git reset --hard"
alias grhm="git reset --hard origin/master"

# Use neovim instead of the built-in vim
alias vim="nvim"
alias vi="nvim"

# Make sudo respect aliases
alias sudo="sudo "

# ------------------------------------- *
# Random Things
# ------------------------------------- *

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# eval "$(rbenv init -)" # Automatically load rbenv
# rbenv automatically creates a shim for the shopify CLI, but I don't want that
# [ -f ~/.rbenv/shims/shopify ] && rm ~/.rbenv/shims/shopify

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
